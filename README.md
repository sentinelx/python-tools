# Python tools

Set of random scripts I write for pushing my knowledge of python and all things infosec.
All information and software available on this repository are for educational purposes only.
Use these at your own discretion, the author of the tools/information cannot be held responsible for any damages caused.

Usage of all tools on this site for attacking targets without prior mutual consent is illegal.
It is the end user’s responsibility to obey all applicable local, state and federal laws.
I assume no liability and are not responsible for any misuse or damage caused by this site.