#!/usr/bin/env python3
#creating a TCP server
"""This server will make a lot of assumptions at first but it is a quick and 
dirt TCP server to send commands to our client."""

import socket
import threading


bind_ip = "0.0.0.0"
bind_port = 15000

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind((bind_ip, bind_port))
server.listen(5)

print("[*] Listening on {}".format({bind_ip, bind_port}))


#client handling thread

def handle_client(client_socket):
	#receives client data
	request = client_socket.recv(1024)
	print("[*] Received {}".format({request}))
	
	#ack packet
	client_socket.send(b"ACK!")
	client_socket.close()
	
while True:
	client,addr = server.accept()
	print("[*] Accepting connection from:{}".format({client}))
	client_handler = threading.Thread(target=handle_client, args=(client,))
	client_handler.start()
